<?php

use Bitrix\Main\Grid\Panel\Snippet;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
{
    die();
}

if (!Loader::includeModule('crm')) {
    return;
}

$asset = Asset::getInstance();
$asset->addJs('/bitrix/js/crm/interface_grid.js');

$rows = array();

$gridManagerId = $arResult['GRID_ID'] . '_MANAGER';

foreach($arResult['FRUITS'] as $fruit){
    $viewUrl = CComponentEngine::makePathFromTemplate(
            $arParams['URL_TEMPLATES']['DETAIL'],
            array('STORE_ID' => $fruit['ID'])
    );

    $editUrl = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['EDIT'],
        array('STORE_ID' => $fruit['ID'])
    );

    $deleteUrlParams = http_build_query(array(
        'action_button_' . $arResult['GRID_ID'] => 'delete',
        'ID' => array($fruit['ID']),
        'sessid' => bitrix_sessid()
    ));
    $deleteUrl = $arParams['SEF_FOLDER'] . '?' . $deleteUrlParams;

    $rows[] = array(
        'id' => $fruit['ID'],
        'actions' => array(
            array(
                'TITLE' => Loc::getMessage('FRUITS_STORE_ACTION_VIEW_TITLE'),
                'TEXT' => Loc::getMessage('FRUITS_STORE_ACTION_VIEW_TEXT'),
                'ONCLICK' => 'BX.Crm.Page.open(' . Json::encode($viewUrl) . ')',
                'DEFAULT' => true
            ),
            array(
                'TITLE' => Loc::getMessage('FRUITS_STORE_ACTION_EDIT_TITLE'),
                'TEXT' => Loc::getMessage('FRUITS_STORE_ACTION_EDIT_TEXT'),
                'ONCLICK' => 'BX.Crm.Page.open(' . Json::encode($editUrl) . ')',
            ),
            array(
                'TITLE' => Loc::getMessage('FRUITS_STORE_ACTION_DELETE_TITLE'),
                'TEXT' => Loc::getMessage('FRUITS_STORE_ACTION_DELETE_TEXT'),
                'ONCLICK' => 'BX.CrmUIGridExtension.processMenuCommand(' . Json::encode($gridManagerId) . ', BX.CrmUIGridMenuCommand.remove, { pathToRemove: ' . Json::encode($deleteUrl) . ' })',
            )
        ),
        'columns' => array(
            'ID' => $fruit['ID'],
            'NAME' => '<a href="' . $viewUrl . '" target="_self">' . $fruit['NAME'] . '</a>',
            'PRICE' => $fruit['PRICE'],
        )
    );
}

$snippet = new Snippet();

$APPLICATION->IncludeComponent(
        'bitrix:crm.interface.grid',
        'titleflex',
        array(
            'GRID_ID' => $arResult['GRID_ID'],
            'HEADERS' => $arResult['HEADERS'],
            'ROWS' => $rows,
            'PAGINATION' => $arResult['PAGINATION'],
            'SHOW_TOTAL_COUNTER' => false,
            'HIDE_FILTER' => true,
            'DISABLE_SEARCH' => true,
            'ACTION_PANEL' => array(
                'GROUPS' => array(
                    array(
                        'ITEMS' => array(
                            $snippet->getRemoveButton(),
                            $snippet->getForAllCheckbox(),
                        )
                    )
                )
            ),
            'EXTENSION' => array(
                'ID' => $gridManagerId,
                'CONFIG' => array(
                    'ownerTypeName' => 'STORE',
                    'gridId' => $arResult['GRID_ID'],
                    'serviceUrl' => $arResult['SERVICE_URL'],
                ),
                'MESSAGES' => array(
                    'deletionDialogTitle' => Loc::getMessage('FRUITS_STORE_DELETE_DIALOG_TITLE'),
                    'deletionDialogMessage' => Loc::getMessage('FRUITS_STORE_DELETE_DIALOG_MESSAGE'),
                    'deletionDialogButtonTitle' => Loc::getMessage('FRUITS_STORE_DELETE_DIALOG_BUTTON'),
                )
            ),
        ),
        $this->getComponent(),
        array('HIDE_ICONS' => 'Y')
);
