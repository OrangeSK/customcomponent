<?php
$MESS['FRUITS_STORE_ACTION_VIEW_TITLE'] = 'Просмотреть товар';
$MESS['FRUITS_STORE_ACTION_VIEW_TEXT'] = 'Просмотреть';
$MESS['FRUITS_STORE_ACTION_EDIT_TITLE'] = 'Редактировать товар';
$MESS['FRUITS_STORE_ACTION_EDIT_TEXT'] = 'Редактировать';
$MESS['FRUITS_STORE_ACTION_DELETE_TITLE'] = 'Удалить товар';
$MESS['FRUITS_STORE_ACTION_DELETE_TEXT'] = 'Удалить';
$MESS['FRUITS_STORE_DELETE_DIALOG_TITLE'] = 'Удалить торговую точку';
$MESS['FRUITS_STORE_DELETE_DIALOG_MESSAGE'] = 'Вы уверены, что хотите удалить выбранную торговую точку?';
$MESS['FRUITS_STORE_DELETE_DIALOG_BUTTON'] = 'Удалить';