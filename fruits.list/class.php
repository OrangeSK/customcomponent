<?php

use Bitrix\Main\Context;
use Bitrix\Main\Grid\Options;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\Web\Uri;
use Pusk\Table\FruitsTable;

class FruitsList extends CBitrixComponent
{
    const GRID_ID = 'FRUITS_STORE_LIST';

    const SUPPORTED_ACTIONS = array('delete');

    private static $headers;

    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);

        self::$headers = array(
            array(
                'id' => 'ID',
                'name' => Loc::getMessage('FRUITS_STORE_HEADERS_ID'),
                'type' => 'int'
            ),
            array(
                'id' => 'NAME',
                'name' => Loc::getMessage('FRUITS_STORE_HEADERS_NAME'),
                'default' => true
            ),
            array(
                'id' => 'PRICE',
                'name' => Loc::getMessage('FRUITS_STORE_HEADERS_PRICE'),
                'type' => 'int',
                'default' => true
            ),
        );
    }

    public function executeComponent(){
        $context = Context::getCurrent();
        $request = $context->getRequest();

        $grid = new Options(self::GRID_ID);

        $this->processGridActions();

        $gridNav = $grid->GetNavParams();
        $pager = new PageNavigation('');
        $pager->setPageSize($gridNav['nPageSize']);
        $pager->setRecordCount(FruitsTable::getCount(['limit']));
        if ($request->offsetExists('page')) {
            $currentPage = $request->get('page');
            $pager->setCurrentPage($currentPage > 0 ? $currentPage : $pager->getPageCount());
        } else {
            $pager->setCurrentPage(1);
        }

        $dbFruits = FruitsTable::getList(array(
            'limit' => $pager->getLimit(),
            'offset' => $pager->getOffset(),
        ));
        $fruits = $dbFruits->fetchAll();

        $requestUri = new Uri($request->getRequestedPage());
        $requestUri->addParams(array('sessid' => bitrix_sessid()));

        $this->arResult = array(
            'FRUITS' => $fruits,
            'GRID_ID' => self::GRID_ID,
            'HEADERS' => self::$headers,
            'PAGINATION' => array(
                'PAGE_NUM' => $pager->getCurrentPage(),
                'ENABLE_NEXT_PAGE' => $pager->getCurrentPage() < $pager->getPageCount(),
                'URL' => $request->getRequestedPage(),
            ),
        );

        $this->includeComponentTemplate();
    }

    private function processGridActions()
    {
        if (!check_bitrix_sessid()) {
            return;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $action = $request->get('action_button_' . self::GRID_ID);

        if (!in_array($action, self::SUPPORTED_ACTIONS)) {
            return;
        }

        $allRows = $request->get('action_all_rows_' . self::GRID_ID) == 'Y';
        if ($allRows) {
            $dbStores = FruitsTable::getList(array(
                'select' => array('ID'),
            ));
            $storeIds = array();
            foreach ($dbStores as $store) {
                $storeIds[] = $store['ID'];
            }
        } else {
            $storeIds = $request->get('ID');
            if (!is_array($storeIds)) {
                $storeIds = array();
            }
        }

        if (empty($storeIds)) {
            return;
        }

        switch ($action) {
            case 'delete':
                foreach ($storeIds as $storeId) {
                    FruitsTable::delete($storeId);
                }
                break;

            default:
                break;
        }
    }
}