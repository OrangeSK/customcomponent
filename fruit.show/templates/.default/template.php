<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/** @var CBitrixComponentTemplate $this */
$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.form',
    'show',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'FORM_ID' => $arResult['FORM_ID'],
        'TACTILE_FORM_ID' => $arResult['TACTILE_FORM_ID'],
        'ENABLE_TACTILE_INTERFACE' => 'Y',
        'SHOW_SETTINGS' => 'Y',
        'DATA' => $arResult['FRUIT'],
        'TABS' => array(
            array(
                'id' => 'tab_1',
                'name' => Loc::getMessage('FRUITS_STORE_TAB_STORE_NAME'),
                'title' => Loc::getMessage('FRUITS_STORE_TAB_STORE_TITLE'),
                'display' => false,
                'fields' => array(
                    array(
                        'id' => 'ID',
                        'name' => Loc::getMessage('FRUITS_STORE_FIELD_ID'),
                        'type' => 'label',
                        'value' => $arResult['FRUIT']['ID'],
                        'isTactile' => true,
                    ),
                    array(
                        'id' => 'NAME',
                        'name' => Loc::getMessage('FRUITS_STORE_FIELD_NAME'),
                        'type' => 'label',
                        'value' => $arResult['FRUIT']['NAME'],
                        'isTactile' => true,
                    ),
                    array(
                        'id' => 'PRICE',
                        'name' => Loc::getMessage('FRUITS_STORE_FIELD_PRICE'),
                        'type' => 'label',
                        'value' => $arResult['FRUIT']['PRICE'],
                        'isTactile' => true,
                    ),
                )
            ),
        ),
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);