<?php

use Bitrix\Main\Localization\Loc;
use Pusk\Table\FruitsTable;

defined('B_PROLOG_INCLUDED') || die;

class FruitShow extends CBitrixComponent
{
    const FORM_ID = 'FRUITS_STORE_SHOW';

    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);

        CBitrixComponent::includeComponentClass('my.crmstores:fruits.list');
        CBitrixComponent::includeComponentClass('my.crmstores:fruit.edit');
    }

    public function executeComponent()
    {
        global $APPLICATION;

        $APPLICATION->SetTitle(Loc::getMessage('FRUITS_STORE_SHOW_TITLE_DEFAULT'));

        $dbStore = FruitsTable::getById($this->arParams['STORE_ID']);
        $fruit = $dbStore->fetch();

        $APPLICATION->SetTitle(Loc::getMessage(
            'FRUITS_STORE_SHOW_TITLE',
            array(
                '#ID#' => $fruit['ID'],
                '#NAME#' => $fruit['NAME']
            )
        ));

        $this->arResult = array(
            'FORM_ID' => self::FORM_ID,
            'TACTILE_FORM_ID' => FruitEdit::FORM_ID,
            'GRID_ID' => FruitsList::GRID_ID,
            'FRUIT' => $fruit
        );

        $this->includeComponentTemplate();
    }
}