<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['FRUITS_STORE_SHOW_TITLE_DEFAULT'] = 'Фрукт';
$MESS['FRUITS_STORE_SHOW_TITLE'] = 'Фрукт №#ID# &mdash; #NAME#';