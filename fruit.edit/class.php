<?php

use Bitrix\Main\Context;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Localization\Loc;
use Pusk\Table\FruitsTable;
use Bitrix\Main\Error;

/**
 * Created by PhpStorm.
 * User: razrabotchik
 * Date: 01.08.2019
 * Time: 14:07
 */

class FruitEdit extends CBitrixComponent
{
    const FORM_ID = 'FRUITS_STORE_EDIT';

    private $errors;

    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);

        $this->errors = new ErrorCollection();

        CBitrixComponent::includeComponentClass('my.crmstores:fruits.list');
    }

    public function executeComponent()
    {
        global $APPLICATION;

        $title = Loc::getMessage('FRUITS_STORE_SHOW_TITLE_DEFAULT');

        $fruit = array(
            'NAME' => '',
            'PRICE' => 0,
        );

        if (intval($this->arParams['STORE_ID']) > 0) {
            $dbStore = FruitsTable::getById($this->arParams['STORE_ID']);
            $fruit = $dbStore->fetch();
        }

        if (!empty($fruit['ID'])) {
            $title = Loc::getMessage(
                'FRUITS_STORE_SHOW_TITLE',
                array(
                    '#ID#' => $fruit['ID'],
                    '#NAME#' => $fruit['NAME']
                )
            );
        }

        $APPLICATION->SetTitle($title);

        if (self::isFormSubmitted()) {
            $savedStoreId = $this->processSave($fruit);
            if ($savedStoreId > 0) {
                LocalRedirect($this->getRedirectUrl($savedStoreId));
            }

            $submittedFruit = $this->getSubmittedFruit();
            $fruit = array_merge($fruit, $submittedFruit);
        }

        $this->arResult =array(
            'FORM_ID' => self::FORM_ID,
            'GRID_ID' => FruitsList::GRID_ID,
            'IS_NEW' => empty($fruit['ID']),
            'TITLE' => $title,
            'FRUIT' => $fruit,
            'BACK_URL' => $this->getRedirectUrl(),
            'ERRORS' => $this->errors,
        );

        $this->includeComponentTemplate();
    }

    private function processSave($initialFruit)
    {
        $submittedFruit = $this->getSubmittedFruit();

        $fruit = array_merge($initialFruit, $submittedFruit);

        $this->errors = self::validate($fruit);

        if (!$this->errors->isEmpty()) {
            return false;
        }

        if (!empty($fruit['ID'])) {
            $result = FruitsTable::update($fruit['ID'], $fruit);
        } else {
            $result = FruitsTable::add($fruit);
        }

        if (!$result->isSuccess()) {
            $this->errors->add($result->getErrors());
        }

        return $result->isSuccess() ? $result->getId() : false;
    }

    private function getSubmittedFruit()
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();

        $submittedFruit = array(
            'NAME' => $request->get('NAME'),
            'PRICE' => $request->get('PRICE'),
        );

        return $submittedFruit;
    }

    private static function validate($fruit)
    {
        $errors = new ErrorCollection();

        if (empty($fruit['NAME'])) {
            $errors->setError(new Error(Loc::getMessage('FRUITS_STORE_ERROR_EMPTY_NAME')));
        }

        if (empty($fruit['PRICE'])) {
            $errors->setError(new Error(Loc::getMessage('FRUITS_STORE_ERROR_EMPTY_PRICE')));
        }

        return $errors;
    }

    private static function isFormSubmitted()
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $saveAndView = $request->get('saveAndView');
        $saveAndAdd = $request->get('saveAndAdd');
        $apply = $request->get('apply');
        return !empty($saveAndView) || !empty($saveAndAdd) || !empty($apply);
    }

    private function getRedirectUrl($savedStoreId = null)
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();

        if (!empty($savedStoreId) && $request->offsetExists('apply')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                array('STORE_ID' => $savedStoreId)
            );
        } elseif (!empty($savedStoreId) && $request->offsetExists('saveAndAdd')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                array('STORE_ID' => 0)
            );
        }

        $backUrl = $request->get('backurl');
        if (!empty($backUrl)) {
            return $backUrl;
        }

        if (!empty($savedStoreId) && $request->offsetExists('saveAndView')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['DETAIL'],
                array('STORE_ID' => $savedStoreId)
            );
        } else {
            return $this->arParams['SEF_FOLDER'];
        }
    }
}