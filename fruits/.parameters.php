<?php

use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => array(
            'details' => array(
                'NAME' => Loc::getMessage('FRUITS_STORE_DETAILS_URL_TEMPLATE'),
                'DEFAULT' => '#STORE_ID#/',
                'VARIABLES' => array('STORE_ID')
            ),
            'edit' => array(
                'NAME' => Loc::getMessage('FRUITS_STORE_EDIT_URL_TEMPLATE'),
                'DEFAULT' => '#STORE_ID#/edit/',
                'VARIABLES' => array('STORE_ID')
            )
        )
    )
);