<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['FRUITS_STORE_SEF_NOT_ENABLED'] = 'Режим ЧПУ должен быть включен.';
$MESS['FRUITS_STORE_SEF_BASE_EMPTY'] = 'Не указан каталог ЧПУ.';