<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['FRUITS_STORE_DETAILS_URL_TEMPLATE'] = 'Шаблон пути к карточке фрукта';
$MESS['FRUITS_STORE_EDIT_URL_TEMPLATE'] = 'Шаблон пути к форме редактирования фрукта';