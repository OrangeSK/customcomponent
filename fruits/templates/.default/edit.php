<?php
/**
 * Created by PhpStorm.
 * User: razrabotchik
 * Date: 31.07.2019
 * Time: 10:18
 */

$urlTemplates = array(
    'DETAIL' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['details'],
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['edit'],
);

$APPLICATION->IncludeComponent(
    'my.crmstores:fruit.edit',
    '',
    array(
        'STORE_ID' => $arResult['VARIABLES']['STORE_ID'],
        'URL_TEMPLATES' => $urlTemplates,
        'SEF_FOLDER' => $arResult['SEF_FOLDER'],
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);