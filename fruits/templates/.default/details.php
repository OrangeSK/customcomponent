<?php
/**
 * Created by PhpStorm.
 * User: razrabotchik
 * Date: 31.07.2019
 * Time: 10:18
 */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Web\Uri;

$urlTemplates = array(
    'LIST' => $arResult['SEF_FOLDER'],
    'DETAIL' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['details'],
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['edit'],
);

$editUrl = CComponentEngine::makePathFromTemplate(
    $urlTemplates['EDIT'],
    array('STORE_ID' => $arResult['VARIABLES']['STORE_ID'])
);

$viewUrl = CComponentEngine::makePathFromTemplate(
    $urlTemplates['DETAIL'],
    array('STORE_ID' => $arResult['VARIABLES']['STORE_ID'])
);

$editUrl = new Uri($editUrl);
$editUrl->addParams(array('backurl' => $viewUrl));

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.toolbar',
    'type2',
    array(
        'TOOLBAR_ID' => 'CRMSTORES_TOOLBAR',
        'BUTTONS' => array(
            array(
                'TEXT' => Loc::getMessage('FRUITS_STORE_BACK'),
                'TITLE' => Loc::getMessage('FRUITS_STORE_BACK'),
                'LINK' => $urlTemplates['LIST'],
                'ICON' => 'btn-',
            ),
            array(
                'TEXT' => Loc::getMessage('FRUITS_STORE_EDIT'),
                'TITLE' => Loc::getMessage('FRUITS_STORE_EDIT'),
                'LINK' => $editUrl->getUri(),
                'ICON' => 'btn-edit',
            ),
        )
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

$APPLICATION->IncludeComponent(
    'my.crmstores:fruit.show',
    '',
    array(
        'STORE_ID' => $arResult['VARIABLES']['STORE_ID']
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);