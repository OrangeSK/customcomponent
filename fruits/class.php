<?php

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Pusk\Table\FruitsTable;

class Fruits extends CBitrixComponent
{
    public function onIncludeComponentLang()
    {
        Loc::loadMessages(__FILE__);
    }

    const SEF_DEFAULT_TEMPLATES = array(
        'details' => '#STORE_ID#/',
        'edit' => '#STORE_ID#/edit/',
    );

    function createDb(){
        $db = Application::getConnection();

        $storeEntity = FruitsTable::getEntity();
        if (! $db->isTableExists($storeEntity->getDBTableName()))
        {
            $storeEntity->createDbTable();
        }
    }

    function deleteDb(){
        $storeConnectionName = FruitsTable::getConnectionName();
        $db = Application::getConnection($storeConnectionName);
        $storeEntity = FruitsTable::getEntity();

        if($db->isTableExists($storeEntity->getDBTableName())) {
            $db->dropTable($storeEntity->getDBTableName());
        }
    }

    public function executeComponent(){
        $this->createDb();

        if(empty($this->arParams['SEF_MODE']) || $this->arParams['SEF_MODE'] != 'Y'){
            ShowError(Loc::getMessage('FRUITS_STORE_SEF_NOT_ENABLED'));
        }
        if(empty($this->arParams['SEF_FOLDER'])){
            ShowError(Loc::getMessage('FRUITS_STORE_SEF_BASE_EMPTY'));
        }
        if(!is_array($this->arParams['SEF_URL_TEMPLATES'])){
            $this->arParams['SEF_URL_TEMPLATES'] = array();
        }

        $sefTemplates = array_merge(self::SEF_DEFAULT_TEMPLATES, $this->arParams['SEF_URL_TEMPLATES']);

        $page = CComponentEngine::parseComponentPath(
            $this->arParams['SEF_FOLDER'],
            $sefTemplates,
            $arVariables
        );

        if (empty($page)) {
            $page = 'list';
        }

        $this->arResult = array(
            'SEF_FOLDER' => $this->arParams['SEF_FOLDER'],
            'SEF_URL_TEMPLATES' => $sefTemplates,
            'VARIABLES' => $arVariables,
        );

        $this->includeComponentTemplate($page);
    }
}